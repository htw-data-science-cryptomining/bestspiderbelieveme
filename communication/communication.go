package communication

var (
	RequestChannel chan Request
	ResponseChannel chan Response
)

type DataCluster struct {
	Date uint64
	Address string
	TopAddress string
	Timeout uint32
	Err uint8
}

type Request struct {
	Data []DataCluster
	Try uint32
	Frame uint64
}

type Response struct {
	ClusterFailed []DataCluster
	ClusterToAdd []DataCluster
	Try uint32
	Frame uint64
}

const (
	ErrNone = 0
	ErrPanic = 1 //404
	ErrSoft = 2 //400
	ErrTimeout = 3 //Obv
)

func Establish() {
	RequestChannel = make(chan Request, 1)
	ResponseChannel = make(chan Response, 1)
}

func NewRequest(cluster []DataCluster, try uint32, frame uint64) Request {
	return Request{
		Data: cluster,
		Try: try,
		Frame: frame,
	}
}

func NewResponse(clusterFailed []DataCluster, clusterToAdd []DataCluster, try uint32, frame uint64) Response {
	return Response{
		ClusterFailed: clusterFailed,
		ClusterToAdd: clusterToAdd,
		Try: try,
		Frame: frame,
	}
}

func CopyFailedFromResponse(response Response) Request {
	return Request{
		Data: response.ClusterFailed,
		Try: (response.Try + 1),
		Frame: response.Frame,
	}
}

func CopyAddFromResponse(response Response, frame uint64) Request {
	return Request{
		Data: response.ClusterToAdd,
		Try: 0,
		Frame: frame,
	}
}

func NewDataCluster(date uint64, address string, topaddress string) DataCluster {
	return DataCluster{
		Date: date,
		Address: address,
		TopAddress: topaddress,
		Timeout: 10,
		Err: ErrNone,
	}
}

func (response *Response) ProcessError() {
	var buffer []DataCluster

	for _, data := range response.ClusterFailed {
		switch data.Err {
		case ErrNone:
			buffer = append(buffer, data)
		case ErrPanic:
			break
		case ErrSoft:
			buffer = append(buffer, data)
		case ErrTimeout:
			data.Timeout = uint32(float32(data.Timeout) * 1.5)
			buffer = append(buffer, data)
		}
	}

	response.ClusterFailed = buffer
}

func (response *Response) CopyNeeded() (bool, bool) {
	failed := false
	add := false

	if len(response.ClusterFailed) > 0 {
		failed = true
	}

	if len(response.ClusterToAdd) > 0 {
		add = true
	}

	return failed, add
}

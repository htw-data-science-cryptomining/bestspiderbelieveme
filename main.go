package main


import (
    "fmt"
    "./communication"
    "./scheduler"
    "./workerPool"
	"./worker/crawler"
    "./waybackcsv"
)

func main() {
	var worker int32
	worker = 60

  communication.Establish()
  fmt.Printf("Communication established\n")

  sched := scheduler.Sched{}
  sched.ReadCfg("config.json")

  reader := waybackcsv.NewReader("waybackmachinedata.csv")
  reader.ReadCfg("config.json")

	crawler.ReadCfg("config.json")

  pool := workerPool.NewWorkerPool(communication.RequestChannel, communication.ResponseChannel)
	fmt.Printf("Worker pool initialized")

  sched.Worker(worker)
  sched.Start()
  fmt.Printf("Scheduler started (%s)\n", sched.TimeStamp.String())

  pool.ResizeWorkers(worker)
  reader.WriteIn(sched.C)

  fmt.Printf("Wait for scheduler to complete\n")
  sched.Wait()
  fmt.Printf("Scheduling completed\n")
  pool.Close()
  fmt.Printf("Worker pool closed\n")
}

package worker

import (
	"sync"
	"fmt"
	"net/url"
	"../communication"
	"./crawler"
)

type Worker struct {
	In chan communication.Request
	Out chan communication.Response
	Done chan struct{}
	wg *sync.WaitGroup
}

func NewWorker(in chan communication.Request, out chan communication.Response, done chan struct{}, wg *sync.WaitGroup) Worker {
	return Worker{
		In: in,
		Out: out,
		Done: done,
		wg: wg,
	}
}

func (w *Worker) Run() {
	defer w.wg.Done()

	cw := crawler.NewCrawler()
	cw.Connect()
	cw.DisableKeepAlives(true)

	for {
		select {
		case req, open := <-w.In:
			if !open {
				return
			}

			fmt.Printf("New Request     - Frame: %d, Try: %d\n", req.Frame, req.Try)

			pages := w.ConvertRequest(req.Data)
			crawled := cw.Crawl(pages)
			clusterFailed, clusterAdd := w.ConvertPages(crawled)

			res := communication.NewResponse(clusterFailed, clusterAdd, req.Try, req.Frame)

			fmt.Printf("New Response    - Frame: %d, Try: %d, Successful (%d/%d), New Links: %d\n", res.Frame, res.Try, len(req.Data) - len(res.ClusterFailed), len(req.Data), len(res.ClusterToAdd))

			w.Out <- res
		case <-w.Done:
			return
		}
	}
}

func (w *Worker) ConvertRequest(data []communication.DataCluster) []*crawler.Webpage {
	var pages []*crawler.Webpage
	var err error

	for _, cluster := range data {
		page := crawler.NewWebpage()

		page.Address, err = url.Parse(cluster.Address)

		if err != nil {
			continue
		}

		page.OriginAddress, err = url.Parse(cluster.TopAddress)

		if err != nil {
			continue
		}

		page.Date = cluster.Date
		page.Timeout = cluster.Timeout
		page.Extracted = false

		pages = append(pages, &page)
	}

	return pages
}

func (w *Worker) ConvertPages(pages []*crawler.Webpage) ([]communication.DataCluster, []communication.DataCluster) {
	var failed []communication.DataCluster
	var add []communication.DataCluster

	for _, page := range pages {
		cluster := communication.NewDataCluster(page.Date, page.Address.String(), page.OriginAddress.String())

		if page.Err != nil && !page.Stored {
			cluster.Err = w.ConvertError(page.Err)
			failed = append(failed, cluster)
			fmt.Printf("Page Failed     - Address: %s, Proxy: %s, Error: %v\n", page.Address, page.Proxy, page.Err)
			continue
		}

		if page.Extracted {
			add = append(add, cluster)
			continue
		}

		fmt.Printf("Page Downloaded - Address: %s, Proxy: %s, Error: %v\n", page.Address, page.Proxy, page.Err)
	}

	return failed, add
}

func (w *Worker) ConvertError(err error) uint8 {
	switch err {
	case crawler.ErrPanic:
		return communication.ErrPanic
	case crawler.ErrTimeout:
		return communication.ErrTimeout
	default:
		return communication.ErrSoft
	}
}

package crawler

import (
	"os"
	"io/ioutil"
	"path/filepath"
	"errors"
	"strings"
	"strconv"
)

var (
	ErrStoreDir = errors.New("Store - Failed to create directories")
	ErrStoreFile = errors.New("Store - Failed to create file")
	ErrStoreWrite = errors.New("Store - Failed to write content")
	ErrStoreSync = errors.New("Store - Failed to sync files")
)


func (cw *Crawler) Directory(page *Webpage) string {
	return filepath.Join(path, page.OriginAddress.String(), strconv.FormatUint(page.Date, 10))
}


func (cw *Crawler) Path(page *Webpage) string {
	return filepath.Join(cw.Directory(page), strconv.FormatUint(uint64(page.Hash), 10) + page.Ext)
}

func (cw *Crawler) Exists(page *Webpage) bool {
  files, err := ioutil.ReadDir(cw.Directory(page))

	if err != nil {
		return false
	}

	for _, file := range files {
		if strings.Contains(file.Name(), strconv.FormatUint(uint64(page.Hash), 10)) {
			return true
		}
	}

	return false
}

func (cw *Crawler) Store(page *Webpage) error {
	err := os.MkdirAll(cw.Directory(page), 0744)

	if err != nil {
		return ErrStoreDir
	}

	file, err := os.Create(cw.Path(page))

	if err != nil {
		return ErrStoreFile
	}

	defer file.Close()

	_, err = file.WriteString(page.Content)

	if err != nil {
		return ErrStoreWrite
	}

	err = file.Sync()

	if err != nil {
		return ErrStoreSync
	}

	return nil
}

package crawler

import (
	"net/url"
	"errors"
	"strings"
	"strconv"
	"fmt"
)

const SandboxBase = "web.archive.org/"

type Webpage struct {
	OriginAddress *url.URL
	Address *url.URL
	Proxy *url.URL
	Date uint64
	Content string
	Media string
	Ext string
	Timeout uint32
	Hash uint32
	Err error
	Extracted bool
	Stored bool
}

const (
	ExtHTML = ".html"
	ExtPHP = ".php"
	ExtJS = ".js"
)

var (
	ErrParsing = errors.New("Address - Url cannot be parsed")
	ErrHostname = errors.New("Address - Url cannot be parsed")
	ErrSandbox = errors.New("Sandbox - Url cannot be parsed")
	ErrPageURL = errors.New("Webpage - Url cannot be parsed")
)

func NewWebpage() Webpage {
	return Webpage{
		OriginAddress: nil,
		Address: nil,
		Date: 0,
		Content: "",
		Media: "",
		Ext: "",
		Timeout: 0,
		Hash: 0,
		Err: nil,
		Stored: false,
	}
}

func (wp *Webpage) IsSandboxed() bool {
	return strings.Contains(wp.Address.String(), SandboxBase)
}

func (wp *Webpage) ToSandbox() error {
	buildURL := "http://" + SandboxBase + "web/" + strconv.FormatUint(wp.Date, 10) + "id_/" + wp.Address.String()

	err := wp.ParseAddress(buildURL)
	if err != nil {
		return ErrSandbox
	}

	return nil
}

func (wp *Webpage) HashAddress() {
	wp.Hash = Hash(wp.Address.String())
}

func (wp *Webpage) ParseAddress(address string) error {
	parsed, err := url.Parse(address)

	if err != nil {
		return ErrPageURL
	}

	wp.Address = parsed
	return nil
}

func (wp *Webpage) ExtensionFromMedia() {
	switch {
	case strings.Contains(wp.Media, "html"):
		wp.Ext = ExtHTML
	case strings.Contains(wp.Media, "javascript"):
		wp.Ext = ExtJS
	case strings.Contains(wp.Media, "php"):
		wp.Ext = ExtPHP
	}
}

func (wp *Webpage) AddMeta() {
	if wp.Ext == ExtJS {
		return
	}

	date := strconv.FormatUint(wp.Date, 10)
	metaDate := "<meta name=\"crawler-date\" content=\"" + fmt.Sprintf("%s-%s-%s", date[0:4], date[4:6], date[6:8]) + "\">"

	wp.Content = strings.Replace(wp.Content, "<head>", "<head>\n" + metaDate, 1)
}

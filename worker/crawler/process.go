package crawler

import (
	"net/url"
	"sync"
	"time"
	"strings"
	"fmt"
	"regexp"
	"hash/fnv"
)


type BlacklistURL struct {
	Address string
	Date uint64
}

type Blacklist struct {
	urls map[BlacklistURL]uint32
	sum uint32
	lock sync.Mutex
}

type Processor struct {
	Links []*url.URL
	EmbeddedScripts []string
	hash uint32
}

func init() {
	blacklist = NewBlacklist()

	go func() {
		for {
			<-time.After(60 * time.Second)
			blacklist.Reduce()
		}
	}()
}

func NewBlacklist() *Blacklist {
	return &Blacklist{
		urls: make(map[BlacklistURL]uint32),
		sum: 0,
		lock: sync.Mutex{},
	}
}

func (bl *Blacklist) Allowed(webpage *Webpage) bool {
	bl.lock.Lock()
	defer bl.lock.Unlock()

	entry := BlacklistURL{
		Address: webpage.Address.String(),
		Date: webpage.Date,
	}

	priority := bl.urls[entry]
	bl.sum++

	if priority == 0 {
		bl.urls[entry] = 1
		return true
	}

	bl.urls[entry]++
	return false
}

func (bl *Blacklist) Reduce() {
	bl.lock.Lock()
	defer bl.lock.Unlock()

	average := float32(bl.sum) / float32(len(bl.urls))

	for key, value := range bl.urls {
		if float32(value) < average {
			delete(bl.urls, key)
			bl.sum = bl.sum - value
		}
	}
}

func NewProcessor() Processor {
	return Processor{
		Links: make([]*url.URL, 0),
		EmbeddedScripts: make([]string, 0),
		hash: 0,
	}
}

func Hash(text string) uint32 {
	hash := fnv.New32a()
	hash.Write([]byte(text))
	return hash.Sum32()
}

func (pro *Processor) ExtractJs(webpage *Webpage) {
	if webpage.Ext == "js" {
		return
	}

	regex := regexp.MustCompile(`<script.*?(?:src=(?:'|")(.+?)(?:'|").*?)?(?:\/>|>([\s\S]*?)<\/script>)`)
	matches := regex.FindAllStringSubmatch(webpage.Content, -1)

	for _, match := range matches {

		if match[1] != "" {
			pro.HandleLink(webpage, match[1])
		} else {
			pro.HandleEmbeddedScript(match[2])
		}

		replacement := fmt.Sprintf(`<script src="%d.js" />`, pro.hash)
		webpage.Content = strings.Replace(webpage.Content, match[0], replacement, 1)
	}
}

func (pro *Processor) HandleLink(webpage *Webpage, link string) {
	pro.hash = Hash(link)

	var buildURL *url.URL
	var err error

	switch {
	case strings.HasPrefix(link, "//"):
		link = "http:" + link
		fallthrough
	case strings.HasPrefix(link, "http"):
		buildURL, err = url.Parse(link)
		if err != nil {
			return
		}
	case strings.HasPrefix(link, "/"):
		buildURL, err = url.Parse(webpage.Address.String() + link)
		if err != nil {
			return
		}
	default:
		return
	}

	pro.Links = append(pro.Links, buildURL)
}

func (pro *Processor) HandleEmbeddedScript(embeddedScript string) {
	pro.hash = Hash(embeddedScript)
	pro.EmbeddedScripts = append(pro.EmbeddedScripts, embeddedScript)
}

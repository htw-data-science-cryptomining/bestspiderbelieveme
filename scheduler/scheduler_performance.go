package scheduler

/*

*/

import (
	"io/ioutil"
	"strings"
	"strconv"
	"time"
)

type SysStats struct {
	CPUUsage float64
	MemUsage float64
	AvCPU float64
	AvMem float64
	CurrCPU float64
	CurrMem float64
	Ticks uint64
}

type CPUStats struct {
	Idle uint64
	Total uint64
	PrevIdle uint64
	PrevTotal uint64
}

type MemStats struct {
	Total uint64
	Free uint64
	Available uint64
}

func (sched *Sched) performance() {
	defer sched.wg.Done()

	ticker := time.NewTicker(100 * time.Millisecond)
	completed := false

	sysStats := SysStats{}
	cpuStats := CPUStats{}
	memStats := MemStats{}

	sysStats.update(cpuStats, memStats)

	for !sched.HasSignalFlag(SigAbort) && !completed {
		select {
		case <- ticker.C:
			sysStats.update(cpuStats, memStats)
			sysStats.Ticks++
		default:
			if sched.HasSignalFlag(SigAbort) {
				ticker.Stop()
				completed = true
			}
		}
	}
}

func (stats *SysStats) update(cpuStats CPUStats, memStats MemStats) error {
	err := cpuStats.read()

	if err != nil {
		return err
	}

	err = memStats.read()

	if err != nil {
		return err
	}

	stats.CurrCPU = cpuStats.usage()
	stats.CurrMem = memStats.usage()

	stats.CPUUsage = stats.CPUUsage + stats.CurrCPU
	stats.MemUsage = stats.MemUsage + stats.CurrMem

	stats.AvCPU = stats.CPUUsage / float64(stats.Ticks)
	stats.AvMem = stats.MemUsage / float64(stats.Ticks)

	return nil
}

func (stats *CPUStats) usage() float64 {
		idle := stats.Idle - stats.PrevIdle
		total := stats.Total - stats.PrevTotal
		return float64(total - idle) / float64(total)
}

func (stats *MemStats) usage() float64 {
		return float64(stats.Total - stats.Available) / float64(stats.Total)
}

func (stats *CPUStats) read() error {
	output, err := ioutil.ReadFile("/proc/stat")

	if err != nil {
		return ErrPFile
	}

	stats.PrevIdle = stats.Idle
	stats.PrevTotal = stats.Total

	lines := strings.Split(string(output), "\n")

	for _, line := range(lines) {
		fields := strings.Fields(line)

		if(len(fields) > 2) {
			switch fields[0] {
			case "cpu":
				/*
					Tcpu = Total CPU time since boot = user+nice+system+idle+iowait+irq+softirq+steal
					Icpu = Total CPU Idle time since boot = idle + iowait
					Ucpu = Total CPU usage time since boot = Tcpu - Icpu
					Total CPU percentage = Ucpu/Tcpu
				*/

				var total uint64 = 0
				var idle uint64 = 0

				for i := 1; i < len(fields); i++ {
					val, err := strconv.ParseUint(fields[i], 10, 64)

					if err != nil {
						return ErrPValue
					}

					total = total + val
				}

				idle, err = strconv.ParseUint(fields[4], 10, 64)

				if err != nil {
					return ErrPValue
				}

				iowait, err := strconv.ParseUint(fields[5], 10, 64)

				if err != nil {
					return ErrPValue
				}

				idle = idle + iowait

				stats.Idle = idle
				stats.Total = total
			}
		}
	}

	return nil
}

func (stats *MemStats) read() error {
	output, err := ioutil.ReadFile("/proc/meminfo")

	if err != nil {
		return ErrPFile
	}

	lines := strings.Split(string(output), "\n")

	for _, line := range(lines) {
		fields := strings.Fields(line)

		if(len(fields) > 1) {
			val, err := strconv.ParseUint(fields[1], 10, 64)

			if err != nil {
				return ErrPValue
			}

			switch fields[0] {
			case "MemTotal:":
				stats.Total = val
			case "MemFree:":
				stats.Free = val
			case "MemAvailable:":
				stats.Available = val
			}
		}
	}

	return nil
}
